﻿using Data.Data;
using Data.Entities;
using Data.Repositories;
using Library.Tests;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TradeMarket.Tests.DataTests
{
    [TestFixture]
    public class ReceiptRepositoryTests
    {
        [TestCase(1)]
        [TestCase(3)]
        public async Task ReceiptRepository_GetById_ReturnsSingleValue(int id)
        {
            using var context = new TradeMarketDbContext(UnitTestHelper.GetUnitTestDbOptions());

            var receiptRepository = new ReceiptRepository(context);
            var receipt = await receiptRepository.GetById(id);

            var expected = ExpectedReceipts.FirstOrDefault(x => x.Id == id);

            Assert.That(receipt, Is.EqualTo(expected).Using(new ReceiptEqualityComparer()), message: "GetById method works incorrect");
        }

        [Test]
        public async Task ReceiptRepository_GetAll_ReturnsAllValues()
        {
            using var context = new TradeMarketDbContext(UnitTestHelper.GetUnitTestDbOptions());

            var receiptRepository = new ReceiptRepository(context);
            var receipts = await receiptRepository.GetAll();

            Assert.That(receipts, Is.EqualTo(ExpectedReceipts).Using(new ReceiptEqualityComparer()), message: "GetAll method works incorrect");
        }

        [Test]
        public async Task ReceiptRepository_Add_AddsValueToDatabase()
        {
            using var context = new TradeMarketDbContext(UnitTestHelper.GetUnitTestDbOptions());

            var receiptRepository = new ReceiptRepository(context);
            var receipt = new Receipt { Id = 4 };

            await receiptRepository.Add(receipt);
            await context.SaveChangesAsync();

            Assert.That(context.Receipts.Count(), Is.EqualTo(4), message: "Add method works incorrect");
        }

        [Test]
        public async Task ReceiptRepository_DeleteById_DeletesEntity()
        {
            using var context = new TradeMarketDbContext(UnitTestHelper.GetUnitTestDbOptions());

            var receiptRepository = new ReceiptRepository(context);

            await receiptRepository.DeleteById(1);
            await context.SaveChangesAsync();

            Assert.That(context.Receipts.Count(), Is.EqualTo(2), message: "DeleteById works incorrect");
        }

        [Test]
        public async Task ReceiptRepository_Update_UpdatesEntity()
        {
            using var context = new TradeMarketDbContext(UnitTestHelper.GetUnitTestDbOptions());

            var receiptRepository = new ReceiptRepository(context);
            var receipt = new Receipt
            {
                Id = 1,
                CustomerId = 2,
                OperationDate = new DateTime(2021, 10, 5),
                IsCheckedOut = false
            };

            receiptRepository.Update(receipt);
            await context.SaveChangesAsync();

            Assert.That(receipt, Is.EqualTo(new Receipt
            {
                Id = 1,
                CustomerId = 2,
                OperationDate = new DateTime(2021, 10, 5),
                IsCheckedOut = false
            }).Using(new ReceiptEqualityComparer()), message: "Update method works incorrect");
        }

        [Test]
        public async Task ReceiptRepository_GetByIdWithDetails_ReturnsWithIncludedEntities()
        {
            using var context = new TradeMarketDbContext(UnitTestHelper.GetUnitTestDbOptions());

            var receiptRepository = new ReceiptRepository(context);

            var receipt = await receiptRepository.GetByIdWithDetails(1);

            var expected = ExpectedReceipts.FirstOrDefault(x => x.Id == 1);
            var expectedReceiptDetailsCount = 2;

            Assert.That(receipt, Is.EqualTo(expected).Using(new ReceiptEqualityComparer()), message: "GetByIdWithDetails method works incorrect");
            Assert.That(receipt.ReceiptDetails.Count, Is.EqualTo(expectedReceiptDetailsCount), message: "GetByIdWithDetails method doesnt't return included entities");
            Assert.That(receipt.Customer, Is.Not.Null, message: "GetByIdWithDetails method doesnt't return included entities");
        }

        [Test]
        public async Task ReceiptRepository_GetAllWithDetails_ReturnsWithIncludedEntities()
        {
            using var context = new TradeMarketDbContext(UnitTestHelper.GetUnitTestDbOptions());

            var receiptRepository = new ReceiptRepository(context);

            var receipts = await receiptRepository.GetAllWithDetails();
            var receipt = receipts.FirstOrDefault(x => x.Id == 1);

            var expectedReceiptDetailsCount = 2;

            Assert.That(receipts, Is.EqualTo(ExpectedReceipts).Using(new ReceiptEqualityComparer()), message: "GetAllWithDetails method works incorrect");
            Assert.That(receipt.ReceiptDetails.Count, Is.EqualTo(expectedReceiptDetailsCount), message: "GetAllWithDetails method doesnt't return included entities");
            Assert.That(receipt.Customer, Is.Not.Null, message: "GetAllWithDetails method doesnt't return included entities");
        }

        private static IEnumerable<Receipt> ExpectedReceipts =>
            new[]
            {
                new Receipt { Id = 1, CustomerId = 1, OperationDate = new DateTime(2021, 7, 5), IsCheckedOut = true },
                new Receipt { Id = 2, CustomerId = 1, OperationDate = new DateTime(2021, 8, 10), IsCheckedOut = true },
                new Receipt { Id = 3, CustomerId = 2, OperationDate = new DateTime(2021, 10, 15), IsCheckedOut = false }
            };
    }
}
