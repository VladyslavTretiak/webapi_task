﻿using Data.Data;
using Data.Entities;
using Data.Repositories;
using Library.Tests;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TradeMarket.Tests.DataTests
{
    [TestFixture]
    public class CustomerRepositoryTests
    {
        [TestCase(1)]
        [TestCase(2)]
        public async Task CustomerRepository_GetById_ReturnsSingleValue(int id)
        {
            using var context = new TradeMarketDbContext(UnitTestHelper.GetUnitTestDbOptions());

            var customerRepository = new CustomerRepository(context);

            var customer = await customerRepository.GetById(id);

            var expected = ExpectedCustomers.FirstOrDefault(x => x.Id == id);

            Assert.That(customer, Is.EqualTo(expected).Using(new CustomerEqualityComparer()), message: "GetById method works incorrect");
        }

        [Test]
        public async Task CustomerRepository_GetAll_ReturnsAllValues()
        {
            using var context = new TradeMarketDbContext(UnitTestHelper.GetUnitTestDbOptions());

            var customerRepository = new CustomerRepository(context);

            var customers = await customerRepository.GetAll();

            Assert.That(customers, Is.EqualTo(ExpectedCustomers).Using(new CustomerEqualityComparer()), message: "GetAll method works incorrect");
        }

        [Test]
        public async Task CustomerRepository_Add_AddsValueToDatabase()
        {
            using var context = new TradeMarketDbContext(UnitTestHelper.GetUnitTestDbOptions());

            var customerRepository = new CustomerRepository(context);
            var customer = new Customer { Id = 3 };

            await customerRepository.Add(customer);
            await context.SaveChangesAsync();

            Assert.That(context.Customers.Count(), Is.EqualTo(3), message: "Add method works incorrect");
        }

        [Test]
        public async Task CustomerRepository_DeleteById_DeletesEntity()
        {
            using var context = new TradeMarketDbContext(UnitTestHelper.GetUnitTestDbOptions());

            var customerRepository = new CustomerRepository(context);

            await customerRepository.DeleteById(1);
            await context.SaveChangesAsync();

            Assert.That(context.Customers.Count(), Is.EqualTo(1), message: "DeleteById works incorrect");
        }

        [Test]
        public async Task CustomerRepository_Update_UpdatesEntity()
        {
            using var context = new TradeMarketDbContext(UnitTestHelper.GetUnitTestDbOptions());

            var customerRepository = new CustomerRepository(context);
            var customer = new Customer
            {
                Id = 1,
                PersonId = 1,
                DiscountValue = 50
            };

            customerRepository.Update(customer);
            await context.SaveChangesAsync();

            Assert.That(customer, Is.EqualTo(new Customer
            {
                Id = 1,
                PersonId = 1,
                DiscountValue = 50
            }).Using(new CustomerEqualityComparer()), message: "Update method works incorrect");
        }

        [Test]
        public async Task CustomerRepository_GetByIdWithDetails_ReturnsWithIncludedEntities()
        {
            using var context = new TradeMarketDbContext(UnitTestHelper.GetUnitTestDbOptions());

            var customerRepository = new CustomerRepository(context);

            var customer = await customerRepository.GetByIdWithDetails(1);

            var expected = ExpectedCustomers.FirstOrDefault(x => x.Id == 1);
            var expectedReceiptsCount = 2;

            Assert.That(customer, Is.EqualTo(expected).Using(new CustomerEqualityComparer()), message: "GetByIdWithDetails method works incorrect");
            Assert.That(customer.Receipts.Count, Is.EqualTo(expectedReceiptsCount), message: "GetByIdWithDetails method doesnt't return included entities");
            Assert.That(customer.Person, Is.Not.Null, message: "GetByIdWithDetails method doesnt't return included entities");
        }

        [Test]
        public async Task CustomerRepository_GetAllWithDetails_ReturnsWithIncludedEntities()
        {
            using var context = new TradeMarketDbContext(UnitTestHelper.GetUnitTestDbOptions());

            var customerRepository = new CustomerRepository(context);

            var customers = await customerRepository.GetAllWithDetails();
            var customer = customers.FirstOrDefault(x => x.Id == 1);

            var expectedReceiptsCount = 2;

            Assert.That(customers, Is.EqualTo(ExpectedCustomers).Using(new CustomerEqualityComparer()), message: "GetAllWithDetails method works incorrect");
            Assert.That(customer.Receipts.Count, Is.EqualTo(expectedReceiptsCount), message: "GetAllWithDetails method doesnt't return included entities");
            Assert.That(customer.Person, Is.Not.Null, message: "GetByIdWithDetails method doesnt't return included entities");
        }

        private static IEnumerable<Customer> ExpectedCustomers =>
            new[]
            {
                new Customer { Id = 1, PersonId = 1, DiscountValue = 20 },
                new Customer { Id = 2, PersonId = 2, DiscountValue = 10 }
            };
    }
}
