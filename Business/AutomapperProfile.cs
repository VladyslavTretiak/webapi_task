﻿using AutoMapper;
using Business.Models;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Product, ProductModel>()
                   .ForMember(pm => pm.ReceiptDetailIds, c => c.MapFrom(pm => pm.ReceiptDetails.Select(x => x.Id)))
                   .ForMember(pm => pm.CategoryName, c => c.MapFrom(x => x.Category.CategoryName))
                   .ReverseMap();

            //TODO: Create mapping for Receipt and ReceiptModel
            CreateMap<Receipt, ReceiptModel>()
                .ForMember(rm => rm.ReceiptDetailsIds, r => r.MapFrom(x => x.ReceiptDetails.Select(rd => rd.Id)))
                .ReverseMap();

            //TODO: Create mapping for ReceiptDetail and ReceiptDetailModel
            CreateMap<ReceiptDetail, ReceiptDetailModel>();

            //TODO: Create mapping that combines Customer and Person into CustomerModel
            CreateMap<Customer, CustomerModel>()
                .ForMember(cm => cm.BirthDate, c => c.MapFrom(x => x.Person.BirthDate))
                .ForMember(cm => cm.Name, cm => cm.MapFrom(x => x.Person.Name))
                .ForMember(cm => cm.Surname, cm => cm.MapFrom(x => x.Person.Surname))
                .ForMember(cm => cm.ReceiptsIds, cm => cm.MapFrom(x => x.Receipts.Select(x => x.Id)))
                .ReverseMap();

            //TODO: Create mapping for ProductCategory and ProductCategoryModel
            CreateMap<ProductCategory, ProductCategoryModel>()
                .ForMember(pcm => pcm.ProductIds, pc => pc.MapFrom(x => x.Products.Select(x => x)))
                .ReverseMap();

            //todo: 
        }
    }
}
