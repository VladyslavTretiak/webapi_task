﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task AddAsync(ProductModel model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.ProductName))
            {
                throw new MarketException(nameof(model));
            }

            var product = _mapper.Map<Product>(model);

            await _unitOfWork.ProductRepository.Add(product);
            await _unitOfWork.SaveAsync();

            model.Id = product.Id;
        }

        public async Task AddCategoryAsync(ProductCategoryModel categoryModel)
        {
            if (categoryModel == null || string.IsNullOrWhiteSpace(categoryModel.CategoryName))
            {
                throw new MarketException(nameof(categoryModel));
            }

            var category = _mapper.Map<ProductCategory>(categoryModel);

            await _unitOfWork.ProductCategoryRepository.Add(category);
            await _unitOfWork.SaveAsync();

            categoryModel.Id = category.Id;
        }

        public async Task DeleteAsync(int modelId)
        {
            await _unitOfWork.ProductRepository.DeleteById(modelId);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<ProductModel>> GetAllAsync()
        {
            // getallwithdetails
            var products = await _unitOfWork.ProductRepository.GetAll();

            return _mapper.Map<IEnumerable<ProductModel>>(products);
        }

        public async Task<IEnumerable<ProductCategoryModel>> GetAllProductCategories()
        {
            var categories = await _unitOfWork.ProductCategoryRepository.GetAll();

            return _mapper.Map<IEnumerable<ProductCategoryModel>>(categories);
        }

        public async Task<IEnumerable<ProductModel>> GetByFilterAsync(FilterSearchModel filterSearch)
        {
            //get all with details
            var products = await _unitOfWork.ProductRepository.GetAll();
        
            if (filterSearch.CategoryId != null)
            {
                products = products.Where(product => product.ProductCategoryId == filterSearch.CategoryId);
            }
            
            if (filterSearch.MinPrice != null)
            {
                products = products.Where(product => product.Price > filterSearch.MinPrice);
            }

            if (filterSearch.MaxPrice != null)
            {
                products = products.Where(product => product.Price < filterSearch.MaxPrice);
            }

            return _mapper.Map<IEnumerable<ProductModel>>(products);
        }

        public async Task<ProductModel> GetByIdAsync(int id)
        {
           var product = await _unitOfWork.ProductRepository.GetById(id);

           return _mapper.Map<ProductModel>(product);
        }

        public async Task RemoveCategoryAsync(int categoryId)
        {
            await _unitOfWork.ProductCategoryRepository.DeleteById(categoryId);
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdateAsync(ProductModel model)
        {
            //add validation
            if (model == null || string.IsNullOrWhiteSpace(model.ProductName))
            {
                throw new MarketException(nameof(model));
            }

            var product = _mapper.Map<Product>(model);

            _unitOfWork.ProductRepository.Update(product);
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdateCategoryAsync(ProductCategoryModel categoryModel)
        {
            //add validation
            if (categoryModel == null || string.IsNullOrWhiteSpace(categoryModel.CategoryName))
            {
                throw new MarketException(nameof(categoryModel));
            }

            var category = _mapper.Map<ProductCategory>(categoryModel);

            _unitOfWork.ProductCategoryRepository.Update(category);
            await _unitOfWork.SaveAsync();
        }
    }
}
