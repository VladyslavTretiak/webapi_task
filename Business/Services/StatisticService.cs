﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Business.Services
{
    public class StatisticService : IStatisticService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public StatisticService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }


        public async Task<IEnumerable<ProductModel>> GetCustomersMostPopularProductsAsync(int productCount, int customerId)
        {
            var receipts = await _unitOfWork.ReceiptRepository.GetAll();

            return receipts.Where(receipt => receipt.CustomerId == customerId)
                .SelectMany(x => x.ReceiptDetails)
                .GroupBy(x => x.ProductId)
                .OrderByDescending(x => x.Sum(receiptDetail=> receiptDetail.Quantity))
                .Take(productCount)
                .Select(x => _mapper.Map<ProductModel>(x.FirstOrDefault()?.Product))
                .ToList();
        }

        public async Task<decimal> GetIncomeOfCategoryInPeriod(int categoryId, DateTime startDate, DateTime endDate)
        {
            var receipts = await _unitOfWork.ReceiptRepository.GetAll();

            var sum = receipts
                .Where(x => x.OperationDate < endDate && x.OperationDate > startDate )
                .Sum(x => x.ReceiptDetails
                    .Where(detail => detail.Product.ProductCategoryId == categoryId)
                    .Sum(detail => detail.Quantity * detail.DiscountUnitPrice)
                );

            return sum;
        }

        public async Task<IEnumerable<ProductModel>> GetMostPopularProductsAsync(int productCount)
        {
            var receiptsDetails = await _unitOfWork.ReceiptDetailRepository.GetAll();

            return receiptsDetails?.GroupBy(x => x.ProductId)
                .OrderByDescending(x => x.Sum(detail => detail.Quantity))
                .Take(productCount)
                .Select(x => _mapper.Map<ProductModel>(x.FirstOrDefault()?.Product))
                .ToList();
        }

        public async Task<IEnumerable<CustomerActivityModel>> GetMostValuableCustomersAsync(int customerCount /*DateTime startDate, DateTime endDate*/)
        {
            var receipts = await _unitOfWork.ReceiptRepository.GetAll();

            return receipts/*Where(x => x.OperationDate > startDate && x.OperationDate < endDate)*/
                .GroupBy(x => x.CustomerId)
                .OrderByDescending(x => x.Sum(receipt => 
                    receipt.ReceiptDetails.Sum(detail => detail.Quantity * detail.DiscountUnitPrice)))
                .Take(customerCount)
                .Select(x => new CustomerActivityModel
                {
                    CustomerId = x.First().CustomerId,
                    CustomerName = $"{x.First().Customer.Person.Name} {x.First().Customer.Person.Surname}",
                    ReceiptSum = x.Sum(r => r.ReceiptDetails.Sum(rd => rd.Quantity * rd.DiscountUnitPrice))
                })
                .ToList();
        }
    }
}
