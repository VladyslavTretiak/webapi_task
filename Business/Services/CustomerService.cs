﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CustomerService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper =  mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task AddAsync(CustomerModel model)
        {
            //add validation 
            if (model == null 
                || string.IsNullOrWhiteSpace(model.Name) 
                || string.IsNullOrWhiteSpace(model.Surname) 
                || model.BirthDate > DateTime.Now 
                || model.BirthDate < new DateTime(1900, 1, 1)
                )
            {
                throw new MarketException(nameof(model));
            }

            var customer = _mapper.Map<Customer>(model);

            await _unitOfWork.CustomerRepository.Add(customer);
            await _unitOfWork.SaveAsync();

            model.Id = customer.Id;
        }

        public async Task DeleteAsync(int modelId)
        {
            await _unitOfWork.CustomerRepository.DeleteById(modelId);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<CustomerModel>> GetAllAsync()
        {
           var customers = await _unitOfWork.CustomerRepository.GetAll();

           return _mapper.Map<IEnumerable<CustomerModel>>(customers);
        }

        public async Task<CustomerModel> GetByIdAsync(int id)
        {
           // getByIdWithDetails
           var customer = await _unitOfWork.CustomerRepository.GetById(id);
        
           return _mapper.Map<CustomerModel>(customer);
        }

        public async Task<IEnumerable<CustomerModel>> GetCustomersByProductIdAsync(int productId)
        {
            var customers = await _unitOfWork.CustomerRepository.GetAll();

            customers = customers.Where(x => x.Receipts.Any(receipt => receipt.ReceiptDetails.Any(receiptDetail => receiptDetail.ProductId == productId)));

            return _mapper.Map<IEnumerable<CustomerModel>>(customers);
        }

        public async Task UpdateAsync(CustomerModel model)
        {
            if (model == null
              || string.IsNullOrWhiteSpace(model.Name)
              || string.IsNullOrWhiteSpace(model.Surname)
              || model.BirthDate > DateTime.Now
              || model.BirthDate < new DateTime(1900, 1, 1)
              )
            {
                throw new MarketException(nameof(model));
            }

            var customer = _mapper.Map<Customer>(model);

            _unitOfWork.CustomerRepository.Update(customer);
            await _unitOfWork.SaveAsync();
        }
    }
}
