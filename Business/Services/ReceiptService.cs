﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Data.Entities;

namespace Business.Services
{
    public class ReceiptService : IReceiptService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ReceiptService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(ReceiptModel model)
        {
            //add validation
            var receipt = _mapper.Map<Receipt>(model);

            await _unitOfWork.ReceiptRepository.Add(receipt);
            await _unitOfWork.SaveAsync();
        }

        /// refactore
        public async Task AddProductAsync(int productId, int receiptId, int quantity)
        {
            var receipt = await _unitOfWork.ReceiptRepository.GetById(receiptId);

            var receiptDetail = receipt.ReceiptDetails.FirstOrDefault(x => x.ProductId == productId);

            if (receiptDetail != null)
            {
                receiptDetail.Quantity += quantity;
            }
            else
            {
                receiptDetail = new ReceiptDetail { ProductId = productId, ReceiptId = receiptId, Quantity = quantity };
                await _unitOfWork.ReceiptDetailRepository.Add(receiptDetail);
            }

            await _unitOfWork.SaveAsync();
        }

        public async Task CheckOutAsync(int receiptId)
        {
            var receipt = await _unitOfWork.ReceiptRepository.GetById(receiptId);
            
            receipt.IsCheckedOut = true;

            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteAsync(int modelId)
        {
           await _unitOfWork.ReceiptRepository.DeleteById(modelId);
           await _unitOfWork.SaveAsync();
        }
 
        public async Task<IEnumerable<ReceiptModel>> GetAllAsync()
        {
            var receipts = await _unitOfWork.ReceiptRepository.GetAll();

            return _mapper.Map<IEnumerable<ReceiptModel>>(receipts);
        }

        public async Task<ReceiptModel> GetByIdAsync(int id)
        {
            var receipt = await _unitOfWork.ReceiptRepository.GetById(id);

            return _mapper.Map<ReceiptModel>(receipt);
        }

        public async Task<IEnumerable<ReceiptDetailModel>> GetReceiptDetailsAsync(int receiptId)
        {
            var receipt = await _unitOfWork.ReceiptRepository.GetById(receiptId);

            return _mapper.Map<IEnumerable<ReceiptDetailModel>>(receipt.ReceiptDetails);
        }

        public async Task<IEnumerable<ReceiptModel>> GetReceiptsByPeriodAsync(DateTime startDate, DateTime endDate)
        {
            var receipts = await _unitOfWork.ReceiptRepository.GetAll();
            var receiptInPeriod = receipts.Where(receipt => receipt.OperationDate > startDate && receipt.OperationDate < endDate).ToList();

            return _mapper.Map<IEnumerable<ReceiptModel>>(receiptInPeriod);
        }

        //check if works with real ef context
        public async Task RemoveProductAsync(int productId, int receiptId, int quantity)
        {
            var receipt = await _unitOfWork.ReceiptRepository.GetById(receiptId);

            var receiptDetail = receipt.ReceiptDetails.FirstOrDefault(x => x.ProductId == productId);

            if (receiptDetail != null)
            {
                if (receiptDetail.Quantity == quantity)
                {
                    _unitOfWork.ReceiptDetailRepository.Delete(receiptDetail);
                }
                else
                {
                    receiptDetail.Quantity -= quantity;
                }
            }

            await _unitOfWork.SaveAsync();
        }

        public async Task<decimal> SumAsync(int receiptId)
        {
            var receipt = await _unitOfWork.ReceiptRepository.GetById(receiptId);

            // 
            return receipt.ReceiptDetails.Sum(orderDetail => orderDetail.Quantity * orderDetail.DiscountUnitPrice);
        }

        public async Task UpdateAsync(ReceiptModel model)
        {
            var receipt = _mapper.Map<Receipt>(model);

            _unitOfWork.ReceiptRepository.Update(receipt);
            await _unitOfWork.SaveAsync();
        }
    }
}
