﻿using Data.Interfaces;
using Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Data.Data
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly TradeMarketDbContext _context;

        private ICustomerRepository _customerRepository;
        private IPersonRepository _personRepository;
        private IProductRepository _productRepository;
        private IProductCategoryRepository _productCategoryRepository;
        private IReceiptRepository _receiptRepository;
        private IReceiptDetailRepository _receiptDetailRepository;

        public UnitOfWork(DbContextOptions<TradeMarketDbContext> options)
        {
            _context = new TradeMarketDbContext(options);
        }

        public ICustomerRepository CustomerRepository
        {
            get
            {

                if (this._customerRepository == null)
                {
                    this._customerRepository = new CustomerRepository(_context);
                }
                return _customerRepository;
            }
        }

        public IPersonRepository PersonRepository
        {
            get
            {

                if (this._personRepository == null)
                {
                    this._personRepository = new PersonRepository(_context);
                }
                return _personRepository;
            }
        }

        public IProductRepository ProductRepository
        {
            get
            {

                if (this._productRepository == null)
                {
                    this._productRepository = new ProductRepository(_context);
                }
                return _productRepository;
            }
        }

        public IProductCategoryRepository ProductCategoryRepository
        {
            get
            {

                if (this._productCategoryRepository == null)
                {
                    this._productCategoryRepository = new ProductCategoryRepository(_context);
                }
                return _productCategoryRepository;
            }
        }

        public IReceiptRepository ReceiptRepository
        {
            get
            {

                if (this._receiptRepository == null)
                {
                    this._receiptRepository = new ReceiptRepository(_context);
                }
                return _receiptRepository;
            }
        }

        public IReceiptDetailRepository ReceiptDetailRepository
        {
            get
            {

                if (this._receiptDetailRepository == null)
                {
                    this._receiptDetailRepository = new ReceiptDetailRepository(_context);
                }
                return _receiptDetailRepository;
            }
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
