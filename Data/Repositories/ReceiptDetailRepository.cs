﻿using Data.Data;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class ReceiptDetailRepository : IReceiptDetailRepository
    {
        readonly TradeMarketDbContext _context;
        readonly DbSet<ReceiptDetail> _dbSet;

        public ReceiptDetailRepository(TradeMarketDbContext context)
        {
            _context = context;
            _dbSet = context.Set<ReceiptDetail>();
        }

        public async Task Add(ReceiptDetail entity)
        {
            if (entity != null)
            {
                await _dbSet.AddAsync(entity);
            }
        }

        public async Task DeleteById(int id)
        {
            ReceiptDetail entity = await _dbSet.FirstOrDefaultAsync(i => i.Id == id);
            if (entity != null)
            {
                _dbSet.Remove(entity);
            }
        }

        public void Delete(ReceiptDetail entity)
        {
            _dbSet.Remove(entity);
        }

        public async Task<IEnumerable<ReceiptDetail>> GetAll()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<ReceiptDetail> GetById(int id)
        {
            return await _dbSet.FirstOrDefaultAsync(i => i.Id == id);
        }

        public void Update(ReceiptDetail entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        public async Task<IEnumerable<ReceiptDetail>> GetAllWithDetails()
        {
            return await _dbSet.Include(i => i.Receipt).Include(i => i.Product).ToListAsync();
        }

        public async Task<ReceiptDetail> GetByIdWithDetails(int id)
        {
            return await _dbSet.Include(i => i.Receipt).Include(i => i.Product).FirstOrDefaultAsync(i => i.Id == id);
        }
    }
}
