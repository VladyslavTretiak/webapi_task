﻿using Data.Data;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        readonly TradeMarketDbContext _context;
        readonly DbSet<Customer> _dbSet;

        public CustomerRepository(TradeMarketDbContext context)
        {
            _context = context;
            _dbSet = context.Set<Customer>();
        }

        public async Task Add(Customer entity)
        {
            if (entity != null)
            {
                await _dbSet.AddAsync(entity);
            }
        }

        public async Task DeleteById(int id)
        {
            Customer entity = await _dbSet.FindAsync(id);
            if (entity != null)
            {
                _dbSet.Remove(entity);
            }
        }

        public void Delete(Customer entity)
        {
            _dbSet.Remove(entity);
        }

        public async Task<IEnumerable<Customer>> GetAll()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<Customer> GetById(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public void Update(Customer entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        public async Task<IEnumerable<Customer>> GetAllWithDetails()
        {
            return await _dbSet.Include(i => i.Receipts).Include(i => i.Person).ToListAsync();
        }

        public async Task<Customer> GetByIdWithDetails(int id)
        {
            return await _dbSet.Include(i => i.Receipts).Include(i => i.Person).FirstOrDefaultAsync(i => i.Id == id);
        }
    }
}
