﻿using Data.Data;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class ReceiptRepository : IReceiptRepository
    {
        readonly TradeMarketDbContext _context;
        readonly DbSet<Receipt> _dbSet;

        public ReceiptRepository(TradeMarketDbContext context)
        {
            _context = context;
            _dbSet = context.Set<Receipt>();
        }

        public async Task Add(Receipt entity)
        {
            if (entity != null)
            {
                await _dbSet.AddAsync(entity);
            }
        }

        public async Task DeleteById(int id)
        {
            Receipt entity = await _dbSet.FindAsync(id);
            if (entity != null)
            {
                _dbSet.Remove(entity);
            }
        }

        public void Delete(Receipt entity)
        {
            _dbSet.Remove(entity);
        }

        public async Task<IEnumerable<Receipt>> GetAll()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<Receipt> GetById(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public void Update(Receipt entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        public async Task<IEnumerable<Receipt>> GetAllWithDetails()
        {
            return await _dbSet.Include(i => i.ReceiptDetails).Include(i => i.Customer).ToListAsync();
        }

        public async Task<Receipt> GetByIdWithDetails(int id)
        {
            return await _dbSet.Include(i => i.ReceiptDetails).Include(i => i.Customer).FirstOrDefaultAsync(i => i.Id == id);
        }
    }
}
