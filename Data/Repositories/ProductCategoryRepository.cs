﻿using Data.Data;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class ProductCategoryRepository : IProductCategoryRepository
    {
        readonly TradeMarketDbContext _context;
        readonly DbSet<ProductCategory> _dbSet;

        public ProductCategoryRepository(TradeMarketDbContext context)
        {
            _context = context;
            _dbSet = context.Set<ProductCategory>();
        }

        public async Task Add(ProductCategory entity)
        {
            if (entity != null)
            {
                await _dbSet.AddAsync(entity);
            }
        }

        public async Task DeleteById(int id)
        {
            ProductCategory entity = await _dbSet.FindAsync(id);
            if (entity != null)
            {
                _dbSet.Remove(entity);
            }
        }

        public void Delete(ProductCategory entity)
        {
            _dbSet.Remove(entity);
        }

        public async Task<IEnumerable<ProductCategory>> GetAll()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<ProductCategory> GetById(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public void Update(ProductCategory entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        public async Task<IEnumerable<ProductCategory>> GetAllWithDetails()
        {
            return await _dbSet.Include(i => i.Products).ToListAsync();
        }

        public async Task<ProductCategory> GetByIdWithDetails(int id)
        {
            return await _dbSet.Include(i => i.Products).FirstOrDefaultAsync(i => i.Id == id);
        }
    }
}
