﻿using Data.Data;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class ProductRepository : IProductRepository
    {
        readonly TradeMarketDbContext _context;
        readonly DbSet<Product> _dbSet;

        public ProductRepository(TradeMarketDbContext context)
        {
            _context = context;
            _dbSet = context.Set<Product>();
        }

        public async Task Add(Product entity)
        {
            if (entity != null)
            {
                await _dbSet.AddAsync(entity);
            }
        }

        public async Task DeleteById(int id)
        {
            Product entity = await _dbSet.FindAsync(id);
            if (entity != null)
            {
                _dbSet.Remove(entity);
            }
        }

        public void Delete(Product entity)
        {
            _dbSet.Remove(entity);
        }

        public async Task<IEnumerable<Product>> GetAll()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<Product> GetById(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public void Update(Product entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        public async Task<IEnumerable<Product>> GetAllWithDetails()
        {
            return await _dbSet.Include(i => i.ReceiptDetails).Include(i => i.Category).ToListAsync();
        }

        public async Task<Product> GetByIdWithDetails(int id)
        {
            return await _dbSet.Include(i => i.ReceiptDetails).Include(i => i.Category).FirstOrDefaultAsync(i => i.Id == id);
        }
    }
}
