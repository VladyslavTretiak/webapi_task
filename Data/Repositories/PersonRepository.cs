﻿using Data.Data;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class PersonRepository : IPersonRepository
    {
        readonly TradeMarketDbContext _context;
        readonly DbSet<Person> _dbSet;

        public PersonRepository(TradeMarketDbContext context)
        {
            _context = context;
            _dbSet = context.Set<Person>();
        }

        public async Task Add(Person entity)
        {
            if (entity != null)
            {
                await _dbSet.AddAsync(entity);
            }
        }

        public async Task DeleteById(int id)
        {
            Person entity = await _dbSet.FindAsync(id);
            if (entity != null)
            {
                _dbSet.Remove(entity);
            }
        }

        public void Delete(Person entity)
        {
            _dbSet.Remove(entity);
        }

        public async Task<IEnumerable<Person>> GetAll()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<Person> GetById(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public void Update(Person entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}
